# fabrik8s

## Mods:
    - https://modrinth.com/mod/lithium
    - https://modrinth.com/mod/c2me-fabric
    - https://modrinth.com/mod/debugify
    - https://modrinth.com/mod/fabric-api
    - https://modrinth.com/mod/ferrite-core
    - https://modrinth.com/mod/krypton
    - https://modrinth.com/mod/lazydfu
    - https://modrinth.com/mod/servercore
    - https://modrinth.com/mod/starlight
    - https://modrinth.com/mod/no-chat-reports
    - https://modrinth.com/mod/bluemap
    - https://modrinth.com/mod/fabricexporter
    - https://modrinth.com/mod/spark
