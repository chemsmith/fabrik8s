FROM amazoncorretto:18 as downloader
ARG MINECRAFT_VERSION=1.19.2
ARG FABRIC_INSTALLER_VERSION=0.11.1
ARG MODS=""

RUN yum install -y --quiet jq

RUN curl -L -o fabric-installer.jar https://maven.fabricmc.net/net/fabricmc/fabric-installer/${FABRIC_INSTALLER_VERSION}/fabric-installer-${FABRIC_INSTALLER_VERSION}.jar && \
    java -jar fabric-installer.jar server -downloadMinecraft -mcversion $MINECRAFT_VERSION

RUN sh -c "mkdir -p mods; cd mods; for mod in ${MODS}; do curl -LO \$(curl --silent \"https://api.modrinth.com/v2/project/\${mod}/version?loaders=[%22fabric%22]&game_versions=[%22${MINECRAFT_VERSION}%22]\" --globoff | jq -r '.[].files[0].url'); done"

FROM amazoncorretto:18 as runtime

WORKDIR /minecraft

COPY --from=downloader /libraries ./libraries
COPY --from=downloader /server.jar ./vanilla.jar
COPY --from=downloader /fabric-server-launch.jar ./server.jar
COPY --from=downloader /mods ./mods

RUN echo "serverJar=/minecraft/vanilla.jar" > fabric-server-launcher.properties && \
    echo "eula=true" > eula.txt

COPY entrypoint.sh /

WORKDIR /data

VOLUME /data

ENTRYPOINT ["/entrypoint.sh"]
