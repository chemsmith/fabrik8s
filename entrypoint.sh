#!/bin/sh

echo "Welcome to another Fabrik8s minecraft server."

ln -fs /minecraft/server.jar .
ln -fs /minecraft/libraries .
ln -fs /minecraft/eula.txt .
ln -fs /minecraft/fabric-server-launcher.properties .

if [ ! "${CUSTOM_MODS}" = "true" ]; then
  echo "You've opted to use the mods supplied with this container. This results in the destruction of your existing mods directory."
  rm -rf mods
  ln -s /minecraft/mods .
fi

if [ -d /minecraft/server-config ] && [ $(find /minecraft/server-config/ -maxdepth 1 \( -type l -o -type f \) ! -iname "..data" -printf x | wc -c) -gt 0 ]; then
  echo "You've mounted server config files, these will overwrite any matching existing files in /data"
  for i in /minecraft/server-config/*; do
    echo "Replacing ${i}"
    rm -f $(basename "$i")
    ln -fs "$i" .
  done
fi

if [ -d /minecraft/mods-config ] && [ $(find /minecraft/server-config/ -maxdepth 1 \( -type l -o -type f \) ! -iname "..data" -printf x | wc -c) -gt 0 ]; then
  echo "You've mounted mod config files, these will overwrite any matching existing files in /data/config"
  for i in /minecraft/mods-config/*; do
    echo "Replacing ${i}"
    rm -f ./config/$(basename "$i")
    ln -fs "$i" ./config/
  done
fi

if [ -n "${RCON_PASSWORD}" ]; then
  if [ -f server.properties ]; then
    sed -i -e "s/^rcon.password=.*/rcon.password=${RCON_PASSWORD}/" server.properties
  else
    echo "rcon.password=${RCON_PASSWORD}" > server.properties
  fi
fi

exec java -Xms4096M -Xmx4096M -Dterminal.jline=false -Dterminal.ansi=true -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:InitiatingHeapOccupancyPercent=15 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true -jar server.jar nogui
